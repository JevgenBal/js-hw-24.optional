const boardSize = 8;
const mineCount = Math.floor((boardSize * boardSize) / 6);

let gameBoard = [];
let mines = [];
let flagsCount = 0;
let openedCount = 0;
let gameInProgress = false;

const gameBoardElement = document.getElementById('gameBoard');
const minesCountElement = document.getElementById('minesCount');
const restartButton = document.getElementById('restartButton');

// Створюємо поле гри
function createGameBoard() {
    gameInProgress = true;
    restartButton.disabled = true;

    // Очищаємо попереднє поле
    gameBoardElement.innerHTML = '';

    // Створюємо нове поле
    gameBoard = [];
    for (let row = 0; row < boardSize; row++) {
        const newRow = [];
        const rowElement = document.createElement('tr');
        for (let col = 0; col < boardSize; col++) {
            const cellElement = document.createElement('td');
            cellElement.addEventListener('click', () => handleCellClick(row, col));
            rowElement.appendChild(cellElement);
            newRow.push({ element: cellElement, hasMine: false, isOpened: false, isFlagged: false });
        }
        gameBoard.push(newRow);
        gameBoardElement.appendChild(rowElement);
    }

    // Розставляємо міни
    mines = [];
    while (mines.length < mineCount) {
        const row = Math.floor(Math.random() * boardSize);
        const col = Math.floor(Math.random() * boardSize);
        if (!gameBoard[row][col].hasMine) {
            gameBoard[row][col].hasMine = true;
            mines.push({ row, col });
        }
    }

    flagsCount = 0;
    openedCount = 0;
    updateMinesCount();
}

// Обробка кліку по комірці
function handleCellClick(row, col) {
    if (!gameInProgress) {
        return;
    }

    const cell = gameBoard[row][col];

    if (cell.isFlagged || cell.isOpened) {
        return;
    }

    if (cell.hasMine) {
        endGame(false);
        return;
    }

    openCell(row, col);

    if (openedCount === (boardSize * boardSize) - mineCount) {
        endGame(true);
    }
}

// Відкриття комірки
function openCell(row, col) {
    const cell = gameBoard[row][col];
    cell.isOpened = true;
    cell.element.classList.add('opened');
    openedCount++;

    if (cell.hasMine) {
        cell.element.classList.add('mine');
        return;
    }

    const minesCount = countNeighborMines(row, col);
    if (minesCount > 0) {
        cell.element.textContent = minesCount;
    } else {
        openNeighborCells(row, col);
    }
}

// Відкриття сусідніх комірок
function openNeighborCells(row, col) {
    for (let i = row - 1; i <= row + 1; i++) {
        for (let j = col - 1; j <= col + 1; j++) {
            if (i >= 0 && i < boardSize && j >= 0 && j < boardSize && !gameBoard[i][j].isOpened) {
                openCell(i, j);
            }
        }
    }
}

// Перевірка чи є міна в комірці
function hasMine(row, col) {
    return row >= 0 && row < boardSize && col >= 0 && col < boardSize && gameBoard[row][col].hasMine;
}

// Підрахунок кількості сусідніх мін
function countNeighborMines(row, col) {
    let count = 0;
    for (let i = row - 1; i <= row + 1; i++) {
        for (let j = col - 1; j <= col + 1; j++) {
            if (hasMine(i, j)) {
                count++;
            }
        }
    }
    return count;
}

// Встановлення або зняття прапорця
function toggleFlag(row, col) {
    if (!gameInProgress) {
        return;
    }

    const cell = gameBoard[row][col];

    if (cell.isOpened) {
        return;
    }

    cell.isFlagged = !cell.isFlagged;
    cell.element.textContent = cell.isFlagged ? '🚩' : '';

    flagsCount += cell.isFlagged ? 1 : -1;
    updateMinesCount();
}

// Оновлення відображення кількості розставлених прапорців
function updateMinesCount() {
    minesCountElement.textContent = `${flagsCount} / ${mineCount}`;
}

// Завершення гри
function endGame(win) {
    gameInProgress = false;
    restartButton.disabled = false;

    for (let row = 0; row < boardSize; row++) {
        for (let col = 0; col < boardSize; col++) {
            const cell = gameBoard[row][col];
            cell.element.removeEventListener('click', () => handleCellClick(row, col));
            cell.element.removeEventListener('contextmenu', (event) => {
                event.preventDefault();
                toggleFlag(row, col);
            });

            if (cell.hasMine && !cell.isOpened) {
                cell.element.textContent = '💣';
            }
        }
    }

    if (win) {
        alert('Ви виграли!');
    } else {
        alert('Ви програли!');
    }
}

// Початок нової гри при натисканні кнопки "Почати гру заново"
restartButton.addEventListener('click', createGameBoard);

// Запуск гри
createGameBoard();
